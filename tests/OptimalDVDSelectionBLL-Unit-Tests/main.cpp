/**
    main.cpp
    Purpose: Defines the entry point for the application.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#define BOOST_TEST_MODULE "OptimalDVDSelectionBLL"
#include "stdafx.h"
#include "test_cdvd.hpp"
#include "test_cdirectory.hpp"
#include "test_cdvdselectioncommandlineparser.h"
#include "test_optimaldvdselectionbll.hpp"

/**
    Simulate, for test purposes, the application's print function when consuming the library.

    @param combination a CCombination object.
    @param combID the ID of the CCombination object.
    @param size the size of the combination (size of all subdirectories in this combination).
*/
void PrintCombination(const std::vector<BLL::CDirectory>& combination, const CCombination::size_type& combID, const double& size)
{
    tstring semicolon(_T(";"));

    // To the standard output, so can be saved as a CSV file, if needed.
    for(CCombination::size_type i = 0U; i < combination.size(); i++)
        tcout << combID << semicolon << combination[i].getDirectory() << semicolon << combination[i].getSize() << semicolon << size << std::endl;
}

namespace BLL{
    CUnitTestSubDirectoriesBuilder::CUnitTestSubDirectoriesBuilder(CDVDSelectionCommandLineParser commandline, const std::vector<CDirectory>& dirs)
        : m_commandline(commandline), m_dirs(dirs.begin(), dirs.end())
    {}

    CUnitTestSubDirectoriesBuilder::~CUnitTestSubDirectoriesBuilder()
    {}

/**
    A custom CSubDirectoriesBuilder::AddSubDirectories so the Unit Test module can simulate internal subdirectories.

    @param subDirs a vector of simulated subdirectories.
*/
    void CUnitTestSubDirectoriesBuilder::AddSubDirectories(std::vector<CDirectory>& subDirs)
    {
        std::copy(this->m_dirs.begin(), this->m_dirs.end(), std::back_inserter(subDirs));
    }
}
