/**
    test_cdvd.hpp
    Purpose: Unit Tests for the CDVD class.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <boost/test/included/unit_test.hpp>
#include "stdafx.h"

BOOST_AUTO_TEST_SUITE(cdvd_suite1)

BOOST_AUTO_TEST_CASE(test_CDVD_defaultvalues)
{
    CDVD cd;

    BOOST_CHECK_EQUAL( cd.getMaxSize(), 4.7*1000.0 );
}

BOOST_AUTO_TEST_SUITE_END()
