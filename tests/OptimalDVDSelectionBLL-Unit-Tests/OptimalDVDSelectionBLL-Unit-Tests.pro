QT     -= gui
QT     -= core

TARGET = OptimalDVDSelectionBLL-Unit-Tests

CONFIG -= qt
CONFIG += console c++11

SOURCES += \
    main.cpp \
    stdafx.cpp
DEFINES += BOOST_ALL_DYN_LINK
DEFINES += BOOST_TEST_DYN_LINK

!win32{
    target.path = /usr/local/bin
    INCLUDEPATH += ../../src/OptimalDVDSelectionBLL
    INSTALLS += target
    LIBS += -lboost_filesystem -lboost_system -lboost_regex -lboost_unit_test_framework
    LIBS += -L../../src/OptimalDVDSelectionBLL/ -lOptimalDVDSelectionBLL
    CONFIG(debug, debug|release){
        QMAKE_CXXFLAGS += --coverage
        QMAKE_LFLAGS += --coverage
    }
    else{
        QMAKE_CXXFLAGS -= --coverage
        QMAKE_LFLAGS -= --coverage
    }
}

HEADERS += \
    stdafx.h \
    test_cdvdselectioncommandlineparser.h \
    test_cdirectory.hpp \
    test_cdvd.hpp \
    test_optimaldvdselectionbll.hpp

PRECOMPILED_HEADER = stdafx.h

