/**
    test_cdirectory.hpp
    Purpose: Unit Tests for the CDirectory class.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <boost/test/included/unit_test.hpp>
#include "stdafx.h"

BOOST_AUTO_TEST_SUITE(cdirectory_suite1)

const TCHAR* const dirs[] = {_T("/usr/lib"), _T("/USR/lib"), _T("uSr/lib"), _T("usr/lIb"), _T("uSr/liB")};

const CDirectory    cd1(200U, dirs[0], 512.0),
                    cd2(200U, dirs[1], 512.0),
                    cd3(200U, dirs[2], 512.0),
                    cd4(200U, dirs[3], 512.0),
                    cd5(200U, dirs[4], 512.0);

BOOST_AUTO_TEST_CASE(test_CDirectory_defaultvalues)
{
    CDirectory cd;

    BOOST_CHECK_EQUAL( cd.getDirectory(), "" );
    BOOST_CHECK_EQUAL( cd.getID(), 0U );
    BOOST_CHECK_EQUAL( cd.getSize(), .0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_constructorvalues)
{
    CDirectory cd(200U, "/usr/lib", 512.0);

    BOOST_CHECK_EQUAL( cd.getDirectory(), "/usr/lib" );
    BOOST_CHECK_EQUAL( cd.getID(), 200U );
    BOOST_CHECK_EQUAL( cd.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison1)
{
    BOOST_CHECK_EQUAL( cd1.getDirectory(), dirs[0] );
    BOOST_CHECK_EQUAL( cd1.getID(), 200U );
    BOOST_CHECK_EQUAL( cd1.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison2)
{
    BOOST_CHECK_EQUAL( cd2.getDirectory(), dirs[1] );
    BOOST_CHECK_EQUAL( cd2.getID(), 200U );
    BOOST_CHECK_EQUAL( cd2.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison3)
{
    BOOST_CHECK_EQUAL( cd3.getDirectory(), dirs[2] );
    BOOST_CHECK_EQUAL( cd3.getID(), 200U );
    BOOST_CHECK_EQUAL( cd3.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison4)
{
    BOOST_CHECK_EQUAL( cd4.getDirectory(), dirs[3] );
    BOOST_CHECK_EQUAL( cd4.getID(), 200U );
    BOOST_CHECK_EQUAL( cd4.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison5)
{
    BOOST_CHECK_EQUAL( cd5.getDirectory(), dirs[4] );
    BOOST_CHECK_EQUAL( cd5.getID(), 200U );
    BOOST_CHECK_EQUAL( cd5.getSize(), 512.0F );
}

BOOST_AUTO_TEST_CASE(test_CDirectory_casesensitivedirectorycomparison6)
{
    BOOST_TEST( (cd1 > cd2) );
}

BOOST_AUTO_TEST_SUITE_END()
