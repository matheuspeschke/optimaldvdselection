/**
    test_cdvdselectioncommandlineparser.hpp
    Purpose: Unit Tests for the CCommandLineParser class.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <boost/test/included/unit_test.hpp>
#include "stdafx.h"

BOOST_AUTO_TEST_SUITE(cdvdselectioncommandlineparser_suite1)

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_defaultOSParameter)
{
    int argc = 1;
    const TCHAR* const argv[] = {gEXEPATH};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    BOOST_REQUIRE_THROW(CDVDSelectionCommandLineParser cdsclp(params), CExceptionCommandLineParser);
}

bool test_CDVDSelectionCommandLineParser_throw_expectedNoDirMessage(const CExceptionCommandLineParser& ex)
{
    BOOST_CHECK_EQUAL(ex.what(), CDVDSelectionCommandLineParser::gExceptionMapping.at(COMMANDLINE_EXCEPTION::COMMANDLINE_EXCEPTION_NO_DIR));
    return true;
}

bool test_CDVDSelectionCommandLineParser_throw_expectedInvalidKMessage(const CExceptionCommandLineParser& ex)
{
    BOOST_CHECK_EQUAL(ex.what(), CDVDSelectionCommandLineParser::gExceptionMapping.at(COMMANDLINE_EXCEPTION::COMMANDLINE_EXCEPTION_INVALID_K));
    return true;
}

bool test_CDVDSelectionCommandLineParser_throw_expectedDuplicatedMessage(const CExceptionCommandLineParser& ex)
{
    BOOST_CHECK_EQUAL(ex.what(), CDVDSelectionCommandLineParser::gExceptionMapping.at(COMMANDLINE_EXCEPTION::COMMANDLINE_EXCEPTION_DUP_PARAM));
    return true;
}

bool test_CDVDSelectionCommandLineParser_throw_expectedHelpMessage(const CExceptionCommandLineParser& ex)
{
    BOOST_CHECK_EQUAL(ex.what(), CDVDSelectionCommandLineParser::gExceptionMapping.at(COMMANDLINE_EXCEPTION::COMMANDLINE_EXCEPTION_HELP_USAGE));
    return true;
}

void test_CDVDSelectionCommandLineParser_DirectoryParameterOnly_throw()
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "/does/not/exist"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

void test_CDVDSelectionCommandLineParser_KParameterOnly_throw()
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "k=1"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

void test_CDVDSelectionCommandLineParser_InvalidKParameterOnly_throw()
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "k=valor"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

void test_CDVDSelectionCommandLineParser_ValidDirOnlyParameter_throw()
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

void test_CDVDSelectionCommandLineParser_InvalidDuplicatedKParameter_throw()
{
    int argc = 4;
    const TCHAR* const argv[] = {gEXEPATH, ".", "k=4", "k=3"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

void test_CDVDSelectionCommandLineParser_ValidHelpParameter_throw()
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "help"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_InvalidDirectoryParameterOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_DirectoryParameterOnly_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedNoDirMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_InvalidDirectoryParameterOnly)
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "/does/not/exist"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    BOOST_REQUIRE_THROW(CDVDSelectionCommandLineParser cdsclp(params), CExceptionCommandLineParser);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_KParameterOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_KParameterOnly_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedNoDirMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_KParameterOnly)
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "k=1"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    BOOST_REQUIRE_THROW(CDVDSelectionCommandLineParser cdsclp(params), CExceptionCommandLineParser);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_InvalidKParameterOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_InvalidKParameterOnly_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedNoDirMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_InvalidKParameterOnly)
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "k=valor"};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    BOOST_REQUIRE_THROW(CDVDSelectionCommandLineParser cdsclp(params), CExceptionCommandLineParser);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_ValidDirOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_ValidDirOnlyParameter_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedInvalidKMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_DuplicatedKOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_InvalidDuplicatedKParameter_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedDuplicatedMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_HelpOnly_ExceptionMessage)
{
    BOOST_CHECK_EXCEPTION(test_CDVDSelectionCommandLineParser_ValidHelpParameter_throw(),
                          CExceptionCommandLineParser,
                          test_CDVDSelectionCommandLineParser_throw_expectedHelpMessage);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_ValidDirOnly)
{
    int argc = 2;
    const TCHAR* const argv[] = {gEXEPATH, "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    BOOST_REQUIRE_THROW(CDVDSelectionCommandLineParser cdsclp(params), CExceptionCommandLineParser);
}

BOOST_AUTO_TEST_CASE(test_CDVDSelectionCommandLineParser_AllParameters_check)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=3", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    CDVDSelectionCommandLineParser cdsclp(params);

    BOOST_TEST(cdsclp.getDirectory() == ".");
    BOOST_TEST(cdsclp.getK() == 3U);
}
BOOST_AUTO_TEST_SUITE_END()