/**
    stdafx.h
    Purpose: Pre-compiled header.

    Reference any additional headers you need here.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

/* Not a real pre-compiled header (for Eclipse). Files are provided for Visual Studio compatibility. */

#ifdef WIN32
#include "targetver.h"
#include <memory>

#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, THIS_FILE, __LINE__)
#define _CRTDBG_MAP_ALLOC 	// include Microsoft memory leak detection procedures.
#define _INC_MALLOC			// exclude standard memory alloc procedures.
#include <crtdbg.h>

#endif
#include <tchar.h>
#endif

#include "CDirectory.hpp"
#include "CDVD.hpp"
#include "CDVDSelectionCommandLineParser.hpp"
#include "OptimalDVDSelectionBLL.hpp"

#include <boost/format.hpp>

using namespace BLL;

namespace BLL{
    const TCHAR* const gEXEPATH = _T("OptimalDVDSelection exe path - supplied by the OS");
}

/**
    Simulate, for test purposes, the application's internal map generated from the command line parameters.

    @param argc the number of parameters in the command line.
    @param argv a pointer to the array of command line parameters.
    @return a map of key (int) and value (tstring) representing the command line parameters.
*/
static const std::map<unsigned int, const tstring> getMap(int argc, const TCHAR* const argv[])
{
    std::map<unsigned int, const tstring> params;
    for(unsigned int i = 0U; i < static_cast<unsigned int>(argc); i++)
        params.insert({i, tstring(argv[i])});

    return params;
}
