/**
    test_optimaldvdselectionbll.hpp
    Purpose: Unit Tests for the COptimalDVDSelection class.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <boost/test/included/unit_test.hpp>
#include "stdafx.h"

namespace BLL{
    class CUnitTestSubDirectoriesBuilder : public CSubDirectoriesBuilder {
    protected:
        CDVDSelectionCommandLineParser m_commandline;
        std::vector<CDirectory> m_dirs;
    public:
        CUnitTestSubDirectoriesBuilder(CDVDSelectionCommandLineParser commandline, const std::vector<CDirectory>& dirs);
        virtual ~CUnitTestSubDirectoriesBuilder();
    public:
        void AddSubDirectories(std::vector<CDirectory>& subDirs);
    };
}

/**
    Helper function. Compare floating point distance using an epsilon.

    @param a the floating point calculated by the application
    @param b the floating point literal to compare with
    @return true (acceptable distance, they are equal) or false (not acceptable distance, they are not equal)
*/
bool dvdSizeAlmostEqual(const double& a, const double& b)
{
    double _abs = std::abs(a - b);
    // Acceptable epsilon for DVD size in megabytes (double ~4700.0).
    double _eps = 0.01; // std::numeric_limits<double>::epsilon();
    BOOST_TEST_MESSAGE(boost::format(_T("Absolute difference of generated DVD size (%f) and provided literal (%f): %f")) % a % b % _abs);
    BOOST_TEST_MESSAGE(boost::format(_T("Acceptable double distance: %f")) % _eps);
    return _abs <= _eps;
}

void PrintCombination(const std::vector<BLL::CDirectory>& combination, const CCombination::size_type& combID, const double& size);

BOOST_AUTO_TEST_SUITE(optimaldvdselectionbll_suite1)

BOOST_AUTO_TEST_CASE(test_OptimalDVDSelectionBLL_memory_2kset_1combination)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=2", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    std::vector<CDirectory> subDirs{
    CDirectory(1U, _T("unittest-dummy-subdir-1"), 1024.0),
    CDirectory(2U, _T("unittest-dummy-subdir-2"), 2048.0),
    CDirectory(3U, _T("unittest-dummy-subdir-3"), 4096.0),
    CDirectory(4U, _T("unittest-dummy-subdir-4"), 1024.0)};

    CDVDSelectionCommandLineParser csclp(params);
    CUnitTestSubDirectoriesBuilder utsdb(csclp, subDirs);

    COptimalDVDSelection opt(csclp, &utsdb);

    auto dirs = opt.getSubDirectories();

    BOOST_TEST ( dirs.size() == subDirs.size() );
    BOOST_TEST ( opt.getK() == 2U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 0U );

    opt.OutputOptimalCombinations( PrintCombination );

    /*
     * https://en.wikipedia.org/wiki/Combination
     *
     * Combinations:
     * S = Number of k combinations
     * k = Number of items per set
     * n = Total number of items
     *
     * ⌈   ⌉
     * | S |        n!
     * |   | = ------------ =
     * | k |     k!(n-k)!
     * ⌊   ⌋
     *
     *          24                      24
     *  --------------------- = --------------------- = 6
     *         2(2)!                    4
     *
     */

    BOOST_TEST ( opt.getOptimalCombination().getDirectoriesIDs().size() == 2U );
    BOOST_TEST ( (dvdSizeAlmostEqual(opt.getOptimalCombination().getSize(), 3072.0)) );
    BOOST_TEST ( opt.getOptimalCombination().getID() == 2U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 6U );

    auto subdir = opt.getSubDirectories();

    BOOST_TEST ( subdir.size() == subDirs.size() );
}

BOOST_AUTO_TEST_CASE(test_OptimalDVDSelectionBLL_memory_3kset_1combination)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=3", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    std::vector<CDirectory> subDirs{
    CDirectory(1U, _T("unittest-dummy-subdir-1"), 1024.0),
    CDirectory(2U, _T("unittest-dummy-subdir-2"), 2048.0),
    CDirectory(3U, _T("unittest-dummy-subdir-3"), 4096.0),
    CDirectory(4U, _T("unittest-dummy-subdir-4"), 1024.0)};

    CDVDSelectionCommandLineParser csclp(params);
    CUnitTestSubDirectoriesBuilder utsdb(csclp, subDirs);

    COptimalDVDSelection opt(csclp, &utsdb);

    auto dirs = opt.getSubDirectories();

    BOOST_TEST ( dirs.size() == subDirs.size() );
    BOOST_TEST ( opt.getK() == 3U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 0U );

    opt.OutputOptimalCombinations( PrintCombination );

    /*
     * https://en.wikipedia.org/wiki/Combination
     *
     * Combinations:
     * S = Number of k combinations
     * k = Number of items per set
     * n = Total number of items
     *
     * ⌈   ⌉
     * | S |        n!
     * |   | = ------------ =
     * | k |     k!(n-k)!
     * ⌊   ⌋
     *
     *          24                      24
     *  --------------------- = --------------------- = 4
     *         6(1)                     6
     *
     */

    BOOST_TEST ( opt.getOptimalCombination().getDirectoriesIDs().size() == 3U );
    BOOST_TEST ( (dvdSizeAlmostEqual(opt.getOptimalCombination().getSize(), 4096.0)) );
    BOOST_TEST ( opt.getOptimalCombination().getID() == 3U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 4U );

    auto subdir = opt.getSubDirectories();

    BOOST_TEST ( subdir.size() == subDirs.size() );
}

BOOST_AUTO_TEST_CASE(test_OptimalDVDSelectionBLL_memory_3kset_8436combinations)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=3", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    std::vector<CDirectory> subDirs{
    CDirectory(1U, _T("unittest-dummy-subdir-1"), 2077.89),
    CDirectory(2U, _T("unittest-dummy-subdir-2"), 295.665),
    CDirectory(3U, _T("unittest-dummy-subdir-3"), 762.28),
    CDirectory(4U, _T("unittest-dummy-subdir-4"), 2101.86),
    CDirectory(5U, _T("unittest-dummy-subdir-5"), 436.911),
    CDirectory(6U, _T("unittest-dummy-subdir-6"), 439.878),
    CDirectory(7U, _T("unittest-dummy-subdir-7"), 1279.98),
    CDirectory(8U, _T("unittest-dummy-subdir-8"), 790.214),
    CDirectory(9U, _T("unittest-dummy-subdir-9"), 1965.09),
    CDirectory(10U, _T("unittest-dummy-subdir-10"), 477.144),
    CDirectory(11U, _T("unittest-dummy-subdir-11"), 1385.44),
    CDirectory(12U, _T("unittest-dummy-subdir-12"), 616.803),
    CDirectory(13U, _T("unittest-dummy-subdir-13"), 1680.65),
    CDirectory(14U, _T("unittest-dummy-subdir-14"), 2014.3),
    CDirectory(15U, _T("unittest-dummy-subdir-15"), 2437.68),
    CDirectory(16U, _T("unittest-dummy-subdir-16"), 613.415),
    CDirectory(17U, _T("unittest-dummy-subdir-17"), 389.74),
    CDirectory(18U, _T("unittest-dummy-subdir-18"), 330.836),
    CDirectory(19U, _T("unittest-dummy-subdir-19"), 645.363),
    CDirectory(20U, _T("unittest-dummy-subdir-20"), 1216.43),
    CDirectory(21U, _T("unittest-dummy-subdir-21"), 933.961),
    CDirectory(22U, _T("unittest-dummy-subdir-22"), 703.198),
    CDirectory(23U, _T("unittest-dummy-subdir-23"), 997.255),
    CDirectory(24U, _T("unittest-dummy-subdir-24"), 1284.99),
    CDirectory(25U, _T("unittest-dummy-subdir-25"), 1253.67),
    CDirectory(26U, _T("unittest-dummy-subdir-26"), 1006.57),
    CDirectory(27U, _T("unittest-dummy-subdir-27"), 1185.51),
    CDirectory(28U, _T("unittest-dummy-subdir-28"), 1026.48),
    CDirectory(29U, _T("unittest-dummy-subdir-29"), 1102.73),
    CDirectory(30U, _T("unittest-dummy-subdir-30"), 328.875),
    CDirectory(31U, _T("unittest-dummy-subdir-31"), 722.444),
    CDirectory(32U, _T("unittest-dummy-subdir-32"), 302.253),
    CDirectory(33U, _T("unittest-dummy-subdir-33"), 224.426),
    CDirectory(34U, _T("unittest-dummy-subdir-34"), 957.482),
    CDirectory(35U, _T("unittest-dummy-subdir-35"), 304.505),
    CDirectory(36U, _T("unittest-dummy-subdir-36"), 184.161),
    CDirectory(37U, _T("unittest-dummy-subdir-37"), 311.884),
    CDirectory(38U, _T("unittest-dummy-subdir-38"), 714.821)};

    CDVDSelectionCommandLineParser csclp(params);
    CUnitTestSubDirectoriesBuilder utsdb(csclp, subDirs);

    COptimalDVDSelection opt(csclp, &utsdb);

    auto dirs = opt.getSubDirectories();

    BOOST_TEST ( dirs.size() == subDirs.size() );
    BOOST_TEST ( opt.getK() == 3U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 0U );

    opt.OutputOptimalCombinations( PrintCombination );

    /*
     * https://en.wikipedia.org/wiki/Combination
     *
     * Combinations:
     * S = Number of k combinations
     * k = Number of items per set
     * n = Total number of items
     *
     * ⌈   ⌉
     * | S |        n!
     * |   | = ------------ =
     * | k |     k!(n-k)!
     * ⌊   ⌋
     *
     *    5,230226175×10⁴⁴        5,230226175×10⁴⁴
     *  --------------------- = --------------------- = 8436
     *         6(38-3)!          6(1,033314797×10⁴⁰)
     *
     */

    BOOST_TEST ( opt.getOptimalCombination().getDirectoriesIDs().size() == 3U );
    BOOST_TEST ( (dvdSizeAlmostEqual(opt.getOptimalCombination().getSize(), 4699.73)) );
    BOOST_TEST ( opt.getOptimalCombination().getID() == 4767U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 8436U );

    auto subdir = opt.getSubDirectories();

    BOOST_TEST ( subdir.size() == subDirs.size() );
}

BOOST_AUTO_TEST_CASE(test_OptimalDVDSelectionBLL_memory_5kset_501942combinations)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=5", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    std::vector<CDirectory> subDirs{
    CDirectory(1U, _T("unittest-dummy-subdir-1"), 2077.89),
    CDirectory(2U, _T("unittest-dummy-subdir-2"), 295.665),
    CDirectory(3U, _T("unittest-dummy-subdir-3"), 762.28),
    CDirectory(4U, _T("unittest-dummy-subdir-4"), 2101.86),
    CDirectory(5U, _T("unittest-dummy-subdir-5"), 436.911),
    CDirectory(6U, _T("unittest-dummy-subdir-6"), 439.878),
    CDirectory(7U, _T("unittest-dummy-subdir-7"), 1279.98),
    CDirectory(8U, _T("unittest-dummy-subdir-8"), 790.214),
    CDirectory(9U, _T("unittest-dummy-subdir-9"), 1965.09),
    CDirectory(10U, _T("unittest-dummy-subdir-10"), 477.144),
    CDirectory(11U, _T("unittest-dummy-subdir-11"), 1385.44),
    CDirectory(12U, _T("unittest-dummy-subdir-12"), 616.803),
    CDirectory(13U, _T("unittest-dummy-subdir-13"), 1680.65),
    CDirectory(14U, _T("unittest-dummy-subdir-14"), 2014.3),
    CDirectory(15U, _T("unittest-dummy-subdir-15"), 2437.68),
    CDirectory(16U, _T("unittest-dummy-subdir-16"), 613.415),
    CDirectory(17U, _T("unittest-dummy-subdir-17"), 389.74),
    CDirectory(18U, _T("unittest-dummy-subdir-18"), 330.836),
    CDirectory(19U, _T("unittest-dummy-subdir-19"), 645.363),
    CDirectory(20U, _T("unittest-dummy-subdir-20"), 1216.43),
    CDirectory(21U, _T("unittest-dummy-subdir-21"), 933.961),
    CDirectory(22U, _T("unittest-dummy-subdir-22"), 703.198),
    CDirectory(23U, _T("unittest-dummy-subdir-23"), 997.255),
    CDirectory(24U, _T("unittest-dummy-subdir-24"), 1284.99),
    CDirectory(25U, _T("unittest-dummy-subdir-25"), 1253.67),
    CDirectory(26U, _T("unittest-dummy-subdir-26"), 1006.57),
    CDirectory(27U, _T("unittest-dummy-subdir-27"), 1185.51),
    CDirectory(28U, _T("unittest-dummy-subdir-28"), 1026.48),
    CDirectory(29U, _T("unittest-dummy-subdir-29"), 1102.73),
    CDirectory(30U, _T("unittest-dummy-subdir-30"), 328.875),
    CDirectory(31U, _T("unittest-dummy-subdir-31"), 722.444),
    CDirectory(32U, _T("unittest-dummy-subdir-32"), 302.253),
    CDirectory(33U, _T("unittest-dummy-subdir-33"), 224.426),
    CDirectory(34U, _T("unittest-dummy-subdir-34"), 957.482),
    CDirectory(35U, _T("unittest-dummy-subdir-35"), 304.505),
    CDirectory(36U, _T("unittest-dummy-subdir-36"), 184.161),
    CDirectory(37U, _T("unittest-dummy-subdir-37"), 311.884),
    CDirectory(38U, _T("unittest-dummy-subdir-38"), 714.821)};

    CDVDSelectionCommandLineParser csclp(params);
    CUnitTestSubDirectoriesBuilder utsdb(csclp, subDirs);

    COptimalDVDSelection opt(csclp, &utsdb);

    auto dirs = opt.getSubDirectories();

    BOOST_TEST ( dirs.size() == subDirs.size() );
    BOOST_TEST ( opt.getK() == 5U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 0U );

    opt.OutputOptimalCombinations( PrintCombination );

    /*
     * https://en.wikipedia.org/wiki/Combination
     *
     * Combinations:
     * S = Number of k combinations
     * k = Number of items per set
     * n = Total number of items
     *
     * ⌈   ⌉
     * | S |        n!
     * |   | = ------------ =
     * | k |     k!(n-k)!
     * ⌊   ⌋
     *
     *    5,230226175×10⁴⁴        5,230226175×10⁴⁴
     *  --------------------- = --------------------- = 501942
     *         120(38-5)!       120(8,683317619×10³⁶)
     *
     */

    BOOST_TEST ( opt.getOptimalCombination().getDirectoriesIDs().size() == 5U );
    BOOST_TEST ( (dvdSizeAlmostEqual(opt.getOptimalCombination().getSize(), 4699.99)) );
    BOOST_TEST ( opt.getOptimalCombination().getID() == 280218U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 501942U );

    auto subdir = opt.getSubDirectories();

    BOOST_TEST ( subdir.size() == subDirs.size() );
}

BOOST_AUTO_TEST_CASE(test_OptimalDVDSelectionBLL_memory_7kset_12620256combinations)
{
    int argc = 3;
    const TCHAR* const argv[] = {gEXEPATH, "k=7", "."};
    const std::map<unsigned int, const tstring> params = getMap(argc, argv);

    std::vector<CDirectory> subDirs{
    CDirectory(1U, _T("unittest-dummy-subdir-1"), 2077.89),
    CDirectory(2U, _T("unittest-dummy-subdir-2"), 295.665),
    CDirectory(3U, _T("unittest-dummy-subdir-3"), 762.28),
    CDirectory(4U, _T("unittest-dummy-subdir-4"), 2101.86),
    CDirectory(5U, _T("unittest-dummy-subdir-5"), 436.911),
    CDirectory(6U, _T("unittest-dummy-subdir-6"), 439.878),
    CDirectory(7U, _T("unittest-dummy-subdir-7"), 1279.98),
    CDirectory(8U, _T("unittest-dummy-subdir-8"), 790.214),
    CDirectory(9U, _T("unittest-dummy-subdir-9"), 1965.09),
    CDirectory(10U, _T("unittest-dummy-subdir-10"), 477.144),
    CDirectory(11U, _T("unittest-dummy-subdir-11"), 1385.44),
    CDirectory(12U, _T("unittest-dummy-subdir-12"), 616.803),
    CDirectory(13U, _T("unittest-dummy-subdir-13"), 1680.65),
    CDirectory(14U, _T("unittest-dummy-subdir-14"), 2014.3),
    CDirectory(15U, _T("unittest-dummy-subdir-15"), 2437.68),
    CDirectory(16U, _T("unittest-dummy-subdir-16"), 613.415),
    CDirectory(17U, _T("unittest-dummy-subdir-17"), 389.74),
    CDirectory(18U, _T("unittest-dummy-subdir-18"), 330.836),
    CDirectory(19U, _T("unittest-dummy-subdir-19"), 645.363),
    CDirectory(20U, _T("unittest-dummy-subdir-20"), 1216.43),
    CDirectory(21U, _T("unittest-dummy-subdir-21"), 933.961),
    CDirectory(22U, _T("unittest-dummy-subdir-22"), 703.198),
    CDirectory(23U, _T("unittest-dummy-subdir-23"), 997.255),
    CDirectory(24U, _T("unittest-dummy-subdir-24"), 1284.99),
    CDirectory(25U, _T("unittest-dummy-subdir-25"), 1253.67),
    CDirectory(26U, _T("unittest-dummy-subdir-26"), 1006.57),
    CDirectory(27U, _T("unittest-dummy-subdir-27"), 1185.51),
    CDirectory(28U, _T("unittest-dummy-subdir-28"), 1026.48),
    CDirectory(29U, _T("unittest-dummy-subdir-29"), 1102.73),
    CDirectory(30U, _T("unittest-dummy-subdir-30"), 328.875),
    CDirectory(31U, _T("unittest-dummy-subdir-31"), 722.444),
    CDirectory(32U, _T("unittest-dummy-subdir-32"), 302.253),
    CDirectory(33U, _T("unittest-dummy-subdir-33"), 224.426),
    CDirectory(34U, _T("unittest-dummy-subdir-34"), 957.482),
    CDirectory(35U, _T("unittest-dummy-subdir-35"), 304.505),
    CDirectory(36U, _T("unittest-dummy-subdir-36"), 184.161),
    CDirectory(37U, _T("unittest-dummy-subdir-37"), 311.884),
    CDirectory(38U, _T("unittest-dummy-subdir-38"), 714.821)};

    CDVDSelectionCommandLineParser csclp(params);
    CUnitTestSubDirectoriesBuilder utsdb(csclp, subDirs);

    COptimalDVDSelection opt(csclp, &utsdb);

    auto dirs = opt.getSubDirectories();

    BOOST_TEST ( dirs.size() == subDirs.size() );
    BOOST_TEST ( opt.getK() == 7U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 0U );

    opt.OutputOptimalCombinations( PrintCombination );

    /*
     * https://en.wikipedia.org/wiki/Combination
     *
     * Combinations:
     * S = Number of k combinations
     * k = Number of items per set
     * n = Total number of items
     *
     * ⌈   ⌉
     * | S |        n!
     * |   | = ------------ =
     * | k |     k!(n-k)!
     * ⌊   ⌋
     *
     *    5,230226175×10⁴⁴        5,230226175×10⁴⁴
     *  --------------------- = --------------------- = 12620256
     *        7!(38-7)!         5040(8,222838654×10³³)
     *
     */

    BOOST_TEST ( opt.getOptimalCombination().getDirectoriesIDs().size() == 7U );
    BOOST_TEST ( (dvdSizeAlmostEqual(opt.getOptimalCombination().getSize(), 4700.0)) );
    BOOST_TEST ( opt.getOptimalCombination().getID() == 4380631U );
    BOOST_TEST ( opt.getNumberOfCombinations() == 12620256U );

    auto subdir = opt.getSubDirectories();

    BOOST_TEST ( subdir.size() == subDirs.size() );
}

BOOST_AUTO_TEST_SUITE_END()