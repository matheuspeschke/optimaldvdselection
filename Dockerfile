FROM ubuntu:bionic

RUN apt-get update
RUN apt-get install -y lcov automake build-essential cmake
RUN apt-get install -y libboost-filesystem-dev libboost-system-dev libboost-regex-dev libboost-test-dev

RUN mkdir -p /opt/optimaldvdselection/dvd/
RUN mkdir -p /opt/optimaldvdselection/build/
RUN mkdir -p /usr/local/lib64/optimaldvdselection/
RUN echo "/usr/local/lib64/optimaldvdselection" | tee /etc/ld.so.conf.d/optimaldvdselection.conf > /dev/null

COPY src /opt/optimaldvdselection/src
COPY tests /opt/optimaldvdselection/tests
COPY CMakeLists.txt /opt/optimaldvdselection/CMakeLists.txt

WORKDIR /opt/optimaldvdselection/build

RUN cmake -DCMAKE_BUILD_TYPE=Release ../.
RUN cmake --build .

RUN cp src/OptimalDVDSelectionBLL/libOptimalDVDSelectionBLL.so.2 /usr/local/lib64/optimaldvdselection
RUN cp src/OptimalDVDSelection/OptimalDVDSelection /usr/local/bin
RUN cp tests/OptimalDVDSelectionBLL-Unit-Tests /usr/local/bin

RUN ldconfig

ENTRYPOINT ["OptimalDVDSelection", "/opt/optimaldvdselection/dvd/"]
CMD ["k=1"]
