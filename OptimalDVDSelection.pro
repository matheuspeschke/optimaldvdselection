TEMPLATE = subdirs

# Project names
SUBDIRS += \
    OptimalDVDSelectionBLL \
    OptimalDVDSelection \
    OptimalDVDSelectionBLL-Unit-Tests

# Resolve solution build dependencies accordingly with the order set on SUBDIRS make variable.
CONFIG += ordered

# Path to the projects
OptimalDVDSelectionBLL.subdir = src/OptimalDVDSelectionBLL
OptimalDVDSelection.subdir = src/OptimalDVDSelection
OptimalDVDSelectionBLL-Unit-Tests.subdir = tests/OptimalDVDSelectionBLL-Unit-Tests

win32{
    QMAKE_CXXFLAGS += /Zi
    QMAKE_LFLAGS += /INCREMENTAL:NO /Debug
}

VERSION = 2.0.0
