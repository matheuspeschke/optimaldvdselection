#!/bin/bash

LCOVBUILDFOLDER=build-gcc

if [ -d "${LCOVBUILDFOLDER}" ]; then
    rm -rf ${LCOVBUILDFOLDER}
fi
mkdir ${LCOVBUILDFOLDER}

cd ${LCOVBUILDFOLDER}

cmake -DUSE_LCOV=true ../.

cmake --build .

# Helps LCOV to locate the .gcda files generated for the external library OptimalDVDSelectionBLL (created when you run the unittests).
export LD_LIBRARY_PATH=${PWD}/src/OptimalDVDSelectionBLL/

# LCOV.
lcov --directory ${PWD} --zerocounters
tests/OptimalDVDSelectionBLL-Unit-Tests --log_level=message
lcov --directory ${PWD} --capture --output-file temp.info

# Remove BOOST and other libraries used by the application.
lcov --remove temp.info "/usr/include/*" "/opt/*" --output-file OptimalDVDSelectionBLL-Unit-Tests.info

# Generates the HTML report.
genhtml OptimalDVDSelectionBLL-Unit-Tests.info --output-directory coveragedir
