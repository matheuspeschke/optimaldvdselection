/**
    CCombination.hpp
    Purpose: A combination instance (k subdirectories)

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "CDirectory.hpp"
#include <vector>

namespace BLL
{
class CCombination
{
public:
    typedef std::vector<CDirectory::size_type>::size_type size_type;
protected:
    std::vector<CDirectory::size_type> m_s;
    double m_size;
    size_type m_id;
public:
    CCombination(std::vector<CDirectory*>& s, size_type id);
    const double& getSize() const;
    const size_type& getID() const;
/**
    Get all the subdirectories IDs that are part of this combination.

    @return a vector of CDirectory::size_type
*/
    const std::vector<CDirectory::size_type>& getDirectoriesIDs() const;
public:
    bool operator>(const CCombination& rhs) const;
};
}
