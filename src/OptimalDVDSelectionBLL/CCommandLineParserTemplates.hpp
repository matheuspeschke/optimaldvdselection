/**
    CCommandLineParserTemplates.hpp
    Purpose: Provide support for templated metaprogramming when parsing various command line parameters.

    @author Matheus Peschke
    @version 2.0.0 2020-05-16
*/
#include "stdafx.h"
#include "CCommandLineParser.hpp"

namespace BLL {

/**
    Generic template to find command line parameters.

    @param parameters a map of all command line parameters.
    @param param the parameter that was found.
    @param pred a generic programming predicate to be used with the std::find_if algorithm.
    @return true (found) or false (not found)
*/
template<typename Predicate>
static bool _find_value_parameter(const CCommandLineParser::type_map& parameters, tstring& param, Predicate pred)
{
    CCommandLineParser::type_map::const_iterator match;

    match = std::find_if(parameters.begin(), parameters.end(), pred);

    if(match != parameters.end())
        param = match->second;

    return match != parameters.end();
}

/**
    Generic template to find and lexically cast a command line parameter's value.

    @param parameters a map of all command line parameters.
    @param param the parameter that was found and lexically cast.
    @param reg the regular expression to find the parameter.
    @return true (found) or false (not found)
*/
template<typename ConvertTo>
static bool _find_name_value_parameter(const CCommandLineParser::type_map& parameters, ConvertTo& param, const tregex& reg)
{
    CCommandLineParser::type_map::const_iterator match;

    match = std::find_if(parameters.begin(), parameters.end(),
            [&](const CCommandLineParser::type_map_pair& p)
            {
                return (boost::regex_match(p.second, reg));
            }
    );

    if(match != parameters.end())
    {
        boost::match_results<tstring::const_iterator> what;

        if(boost::regex_search(match->second.begin(), match->second.end(), what, reg, boost::match_default))
        {
            tstring res(what[static_cast<int>(what.size())-1].first, what[static_cast<int>(what.size())-1].second);
            param = boost::lexical_cast<ConvertTo>(res);
        }
    }

    return match != parameters.end();
}

} /* namespace BLL */