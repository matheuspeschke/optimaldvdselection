/**
    CException.cpp
    Purpose: Exceptions for the BLL namespace.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CException.hpp"

namespace BLL {

CException::CException(const std::string& what)
: m_exception(what)
{}

CException::~CException()
{}

const char* CException::what() const throw()
{return this->m_exception.c_str();}

} /* namespace BLL */
