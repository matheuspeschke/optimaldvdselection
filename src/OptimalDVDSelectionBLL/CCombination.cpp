/**
    CCombination.cpp
    Purpose: A combination instance (k subdirectories)

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "CCombination.hpp"
#include <algorithm>

namespace BLL
{

CCombination::CCombination(std::vector<CDirectory*>& s, size_type id)
    : m_size(0.0), m_id(id)
{
    for(std::vector<CDirectory*>::const_iterator it = s.begin(); it != s.end(); it++)
    {
        this->m_s.push_back((*it)->getID());
        this->m_size += (*it)->getSize();
    }
}

const double& CCombination::getSize() const
{return this->m_size;}

const CCombination::size_type& CCombination::getID() const
{return this->m_id;}

// Get all the subdirectories IDs that are part of this combination.
const std::vector<CDirectory::size_type>& CCombination::getDirectoriesIDs() const
{return this->m_s;}

bool CCombination::operator>(const CCombination& rhs) const
{return this->getSize() > rhs.getSize();}

}
