/**
    CDVDSelectionCommandLineParser.hpp
    Purpose: Manage parsing of command line arguments

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#include "CException.hpp"
#include "CCommandLineParser.hpp"
#include "OptimalDVDSelectionBLL_Global.hpp"
#include "CCombination.hpp"
#include <boost/regex.hpp>

namespace BLL {

typedef boost::basic_regex<TCHAR> tregex;

enum OptimalCombinationsOutputType
{
    OptimalCombinationsOutputType_Memory,
    OptimalCombinationsOutputType_Unknown
};

enum COMMANDLINE_EXCEPTION
{
    COMMANDLINE_EXCEPTION_DUP_PARAM,
    COMMANDLINE_EXCEPTION_NO_DIR,
    COMMANDLINE_EXCEPTION_INVALID_K,
    COMMANDLINE_EXCEPTION_NO_PARAMETERS,
    COMMANDLINE_EXCEPTION_HELP_USAGE
};

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CExceptionCommandLineParser : public CException
{
public:
    CExceptionCommandLineParser(const std::string what);
};

class CDVDSelectionCommandLineParser: public CCommandLineParser
{
public:
    static const std::vector<tregex> gCommandLineRegExps;
    static const std::map<COMMANDLINE_EXCEPTION, const std::string> gExceptionMapping;
private:
    tstring m_directory;
    CCombination::size_type m_k;
    OptimalCombinationsOutputType m_type;
private:
/**
    Helper function (see CDVDSelectionCommandLineParser::AreDuplicated). Finds if a specific command line parameter is duplicated.

    @param reg the regular expression to find the parameter.
    @return true (found) or false (not found)
*/
    bool IsDuplicated(const tregex& reg) const;
/**
    Finds duplicated command line parameters.

    @return true (found) or false (not found)
*/
    bool AreDuplicated(void) const;
/**
    Parses the command line argument equivalent to the Directory.

    @return true (found) or false (not found)
*/
    bool FindDirectory(void);
/**
    Parses the command line argument equivalent to the k parameter.

    @param reg the regular expression to extract the help parameter.
    @return true (found) or false (not found)
*/
    bool FindK(const tregex& reg);
/**
    Parses the command line argument equivalent to the help parameter.

    @param reg the regular expression to extract the help parameter.
    @return true (found) or false (not found)
*/
    bool FindHelp(const tregex& reg);
public:
/**
    Parses all command line arguments

    @return none
*/
    virtual void Parse(void);
    const tstring& getDirectory(void) const;
    const CCombination::size_type& getK(void) const;
    const OptimalCombinationsOutputType& getType(void) const;
public:
    CDVDSelectionCommandLineParser(const std::map<unsigned int, const tstring>& args);
    virtual ~CDVDSelectionCommandLineParser();
};

} /* namespace BLL */
