/**
    CSubDirectoriesBuilder.hpp
    Purpose: superclass for listing and managing subdirectories.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <vector>
#include "CDirectory.hpp"

namespace BLL {

class CSubDirectoriesBuilder {
public:
    virtual void AddSubDirectories(std::vector<CDirectory>& subDirs) = 0;
    virtual ~CSubDirectoriesBuilder();
};

}