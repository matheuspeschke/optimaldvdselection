/**
    CException.hpp
    Purpose: Exceptions for the BLL namespace.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "OptimalDVDSelectionBLL_Global.hpp"
#include <string>

namespace BLL {

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CException : public std::exception
{
private:
    std::string m_exception;
public:
    CException(const std::string& what);
    virtual ~CException();
public:
    virtual const char* what() const throw();
};

} /* namespace BLL */
