/**
    dllmain.cpp
    Purpose: Defines the entry point for the DLL application.

    Only for Windows.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"

#ifdef WIN32
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
#endif

