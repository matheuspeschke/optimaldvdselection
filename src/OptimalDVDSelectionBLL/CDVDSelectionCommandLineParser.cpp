/**
    CDVDSelectionCommandLineParser.cpp
    Purpose: Manage parsing of command line arguments

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CDVDSelectionCommandLineParser.hpp"
#include "CPredDirectoryExists.hpp"
#include "CPredHelp.hpp"
#include "CCommandLineParserTemplates.hpp"

namespace BLL {

static const char *const pHELPUSAGE = " \n\
Given a directory, prints the optimal combination of subdirectories to \n\
burn a DVD ('optimal' = combination that minimizes DVD unused space).\n\
\n\
For a definition of 'combination', see: https://en.wikipedia.org/wiki/Combination\n\
\n\
Combinations:\n\
S = Number of k combinations\n\
k = Number of items per set\n\
n = Total number of items\n\
\n\
⌈   ⌉\n\
| S |        n!\n\
|   | = ------------\n\
| k |     k!(n-k)!\n\
⌊   ⌋\n\
\n\
usage: OptimalDVDSelection \n\
        <directory path>\n\
        k=[k] \n\
\n\
k: \n\
    a positive integer number \n\
    this is the number of subdirectories to use in the DVD\n\
\n\
";

const std::vector<tregex> CDVDSelectionCommandLineParser::gCommandLineRegExps = {
        tregex(_T("\\Ak=(\\d{1,})\\z")),
        tregex(_T("\\Ahelp\\z"))
};

const std::map<COMMANDLINE_EXCEPTION, const std::string> CDVDSelectionCommandLineParser::gExceptionMapping = {
        {COMMANDLINE_EXCEPTION_DUP_PARAM, "There are duplicated command line parameters. Type 'OptimalDVDSelection help' for help."},
        {COMMANDLINE_EXCEPTION_NO_DIR, "No valid directory was found. Type 'OptimalDVDSelection help' for help."},
        {COMMANDLINE_EXCEPTION_INVALID_K, "K is invalid. Type 'OptimalDVDSelection help' for help."},
        {COMMANDLINE_EXCEPTION_NO_PARAMETERS, "No command line parameters found. Type 'OptimalDVDSelection help' for help."},
        {COMMANDLINE_EXCEPTION_HELP_USAGE, pHELPUSAGE}
};

CExceptionCommandLineParser::CExceptionCommandLineParser(const std::string what)
: CException(what)
{}

CDVDSelectionCommandLineParser::CDVDSelectionCommandLineParser(const std::map<unsigned int, const tstring>& args)
    : CCommandLineParser::CCommandLineParser(args), m_k(0U), m_type(OptimalCombinationsOutputType_Memory)
{this->Parse();}

CDVDSelectionCommandLineParser::~CDVDSelectionCommandLineParser()
{}

// Parses all command line arguments
void CDVDSelectionCommandLineParser::Parse(void)
{
    if(this->FindHelp(gCommandLineRegExps[1]))
        throw CExceptionCommandLineParser(gExceptionMapping.at(COMMANDLINE_EXCEPTION_HELP_USAGE));

    if(!this->FindDirectory())
        throw CExceptionCommandLineParser(gExceptionMapping.at(COMMANDLINE_EXCEPTION_NO_DIR));

    if(!this->FindK(gCommandLineRegExps[0]))
        throw CExceptionCommandLineParser(gExceptionMapping.at(COMMANDLINE_EXCEPTION_INVALID_K));

    if(this->AreDuplicated())
        throw CExceptionCommandLineParser(gExceptionMapping.at(COMMANDLINE_EXCEPTION_DUP_PARAM));
}

// Parses the command line argument equivalent to the Directory.
bool CDVDSelectionCommandLineParser::FindDirectory(void)
{
    CPredDirectoryExists pred({0, _T("")});
    return BLL::_find_value_parameter(this->m_parameters, this->m_directory, pred);
}

// Parses the command line argument equivalent to the help parameter.
bool CDVDSelectionCommandLineParser::FindHelp(const tregex& reg)
{
    CPredHelp pred({0, _T("")});
    tstring str;
    return BLL::_find_value_parameter(this->m_parameters, str, pred);
}

// Parses the command line argument equivalent to the k parameter.
bool CDVDSelectionCommandLineParser::FindK(const tregex& reg)
{return BLL::_find_name_value_parameter(this->m_parameters, this->m_k, reg);}

// Finds duplicated command line parameters.
bool CDVDSelectionCommandLineParser::AreDuplicated(void) const
{
    return (std::any_of(gCommandLineRegExps.begin(), gCommandLineRegExps.end(),
            [&](const tregex& reg)
            {return this->IsDuplicated(reg);}
    ));
}

// Helper function (see CDVDSelectionCommandLineParser::AreDuplicated). Finds if a specific command line parameter is duplicated.
bool CDVDSelectionCommandLineParser::IsDuplicated(const tregex& reg) const
{
    CCommandLineParser::type_map::iterator::difference_type duplicates(0);

    duplicates = std::count_if(this->m_parameters.begin(), this->m_parameters.end(),
            [&](const CCommandLineParser::type_map_pair& param)
            {return (boost::regex_match(param.second, reg));}
    );

    return duplicates > 1;
}

const tstring& CDVDSelectionCommandLineParser::getDirectory(void) const
{ return this->m_directory; }

const CCombination::size_type& CDVDSelectionCommandLineParser::getK(void) const
{ return this->m_k; }

const OptimalCombinationsOutputType& CDVDSelectionCommandLineParser::getType(void) const
{ return this->m_type; }

} /* namespace BLL */
