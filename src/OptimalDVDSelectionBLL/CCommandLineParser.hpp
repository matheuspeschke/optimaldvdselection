/**
    CCommandLineParser.hpp
    Purpose: Superclass for managing parse of command line arguments

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "OptimalDVDSelectionBLL_Global.hpp"
#include "tchar_templates.hpp"
#include <map>

namespace BLL {

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CCommandLineParser {
public:
    typedef unsigned int type_map_key;
    typedef const tstring type_map_value;
    typedef std::map<type_map_key, type_map_value> type_map;
    typedef std::pair<type_map_key, type_map_value> type_map_pair;
protected:
    type_map m_parameters;
public:
    CCommandLineParser(const std::map<unsigned int, const tstring>& args);
    virtual ~CCommandLineParser();
public:
    virtual void Parse(void) = 0;
};

} /* namespace BLL */
