/**
    stdafx.h
    Purpose: Pre-compiled header.

    Reference any additional headers you need here.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#ifdef WIN32
#pragma warning( disable : 4251 )
#endif

/* Not a real pre-compiled header (for Eclipse). Files are provided for Visual Studio compatibility. */

#include <iostream>
#include <fstream>

#ifdef _WINDOWS
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <tchar.h>
#include <string>
#endif

#include "tchar_templates.hpp"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

namespace BLL {

#ifdef _WINDOWS
const tstring gDirSeparator = _T("\\");
#else
const tstring gDirSeparator = _T("/");
#endif
}
