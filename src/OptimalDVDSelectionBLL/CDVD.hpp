/**
    CDVD.hpp
    Purpose: inform the max size of a DVD media (http://en.wikipedia.org/wiki/DVD).

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#include "OptimalDVDSelectionBLL_Global.hpp"

namespace BLL {

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CDVD {
public:
/**
    Max size of a DVD disk.

    @return max size of a DVD disk (double)
*/
    static const double& getMaxSize(void);
public:
    CDVD();
    virtual ~CDVD();
};

} /* namespace BLL */
