/**
    CFileSystemSubDirectoriesBuilder.cpp
    Purpose: identifies and sizes the subdirectories inside a directory (non recursive).

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CFileSystemSubDirectoriesBuilder.hpp"

namespace BLL {

CFileSystemSubDirectoriesBuilder::CFileSystemSubDirectoriesBuilder(CDVDSelectionCommandLineParser commandline)
    : m_commandline(commandline)
{}

// Find and size all subdirectories inside a directory.
void CFileSystemSubDirectoriesBuilder::AddSubDirectories(std::vector<CDirectory>& subDirs)
{
    boost::filesystem::path p(this->m_commandline.getDirectory());
    CCombination::size_type ID = 1U;

    // Lists all the sub-directories:
    for(boost::filesystem::directory_iterator iter(p), end; iter != end; ++iter)
        if(boost::filesystem::is_directory(*iter))
        {
            double size=0.0;

            // Traverse all the sub-directories files and folders and sum file sizes:
            for(boost::filesystem::recursive_directory_iterator rec(*iter); rec!=boost::filesystem::recursive_directory_iterator(); ++rec)
                if(!is_directory(*rec))
                    size+=static_cast<double>(boost::filesystem::file_size(*rec)) / static_cast<double>(pow(1000.0, 2)); // For some bizarre reason, most media burners forget to use 2^10 (1024) when multiplying bytes. They use 1000 instead;

            std::vector<tstring> paths;
            tstring fullpath(iter->path().c_str());

            boost::split(paths, fullpath, boost::is_any_of(BLL::gDirSeparator), boost::token_compress_on);

            subDirs.push_back( CDirectory(ID++, paths[paths.size()-1], size) );
        }
}

}