/**
    CPredDirectoryExists.hpp
    Purpose: predicate to assess whether or not a command line parameter corresponds to a real filesystem directory.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "CCommandLineParser.hpp"
#include "boost/filesystem.hpp"

namespace BLL
{

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CPredDirectoryExists
{
private:
    const CCommandLineParser::type_map_pair& m_candidate;
private:
    CPredDirectoryExists();
public:
    CPredDirectoryExists(const CCommandLineParser::type_map_pair& candidate);
    bool operator()(void);
/**
    Function operator - validate if command line parameter is a directory.

    @param candidate a command line parameter that might be a directory
    @return true (candidate is a diretory) or false (candidate is not a diretory)
*/
    bool operator()(const CCommandLineParser::type_map_pair& candidate);
};

}
