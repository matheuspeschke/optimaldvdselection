/**
    OptimalDVDSelectionBLL.hpp
    Purpose: Orchestrate the execution of this program: parse the input (command line parameters), find the subdirectories and determine the optimal combination.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#include "CException.hpp"
#include "OptimalDVDSelectionBLL_Global.hpp"
#include "CDirectory.hpp"
#include "CDVDSelectionCommandLineParser.hpp"
#include "CCombination.hpp"
#include "CSubDirectoriesBuilder.hpp"
#include <string>
#include <vector>
#include <fstream>

namespace BLL {

typedef void (*pfnPrintCombination)(const std::vector<CDirectory>&, const CCombination::size_type& combID, const double& combSize);

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT COptimalDVDSelection {
protected:
    CDVDSelectionCommandLineParser m_CommandLine;
    std::vector<CDirectory> m_SubDirectories;
    pfnPrintCombination m_pfn;
    CCombination::size_type m_NumberOfCombinations;
    CSubDirectoriesBuilder* m_psdb;
    CCombination m_optimal;
public:
    COptimalDVDSelection(const CDVDSelectionCommandLineParser& dsclp, CSubDirectoriesBuilder* psdb);
    virtual ~COptimalDVDSelection();
public:
/**
    Print the optimal combination using the pfnPrintCombination function.

    @param pfn print function provided by the process consuming this library.
*/
    void OutputOptimalCombinations(pfnPrintCombination pfn = nullptr);
/**
    Return a vector of all subdirectories inside a directory.

    @return vector of CDirectoy objects.
*/
    const std::vector<CDirectory>& getSubDirectories(void) const;
/**
    Return the k elements used for each combination.

    @return the number of k elements.
*/
    const CCombination::size_type& getK(void) const;
/**
    Return the number of generated combinations.

    @return the number of combinations.
*/
    const CCombination::size_type& getNumberOfCombinations(void) const;
/**
    Return the object CCombination that is the optimal combination.

    @return the optimal CCombination object.
*/
    const CCombination& getOptimalCombination() const;
protected:
/**
    Find and print the optimal combination of k elements using the pfnPrintCombination function.

    @param k k elements for a combination
    @param pfn print function provided by the process consuming this library.
*/
    void findOptimalCombination(const CCombination::size_type& k, pfnPrintCombination pfn);
/**
    Generate all possible combinations of k elements.

    @param k k elements for a combination
    @param combinations store all generated combinations
*/
    void generateCombinations(const CCombination::size_type& k, std::vector<CCombination>& combinations);
};

} /* namespace BLL */
