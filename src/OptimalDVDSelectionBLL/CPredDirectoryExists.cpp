/**
    CPredDirectoryExists.cpp
    Purpose: predicate to assess whether or not a command line parameter corresponds to a real filesystem directory.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CPredDirectoryExists.hpp"

namespace BLL
{

CPredDirectoryExists::CPredDirectoryExists(const CCommandLineParser::type_map_pair& candidate)
: m_candidate(candidate)
{}

static bool _checkpath(const CCommandLineParser::type_map_pair& candidate)
{
    boost::filesystem::path p(candidate.second);

    return boost::filesystem::is_directory(p);
}

bool CPredDirectoryExists::operator()(void)
{ return _checkpath(this->m_candidate); }

// Function operator - validate if command line parameter is a directory.
bool CPredDirectoryExists::operator()(const CCommandLineParser::type_map_pair& candidate)
{ return _checkpath(candidate); }

}
