/**
    CPredSameDirectory.hpp
    Purpose: Predicate to compare two directories by their IDs.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "CDirectory.hpp"

namespace BLL
{

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CPredSameDirectory
{
private:
    const CDirectory& m_candidate;
private:
    CPredSameDirectory();
public:
    CPredSameDirectory(const CDirectory& candidate);
/**
    Function operator - compare two CDirectory objects by their IDs.

    @param candidate a CDirectory object
    @return true (candidate is equal to this CDirectory object) or false (candidate is not equal to this CDirectory object)
*/
    bool operator()(const CDirectory& candidate);
};

}
