/**
    OptimalDVDSelectionBLL.cpp
    Purpose: Orchestrate the execution of this program: parse the input (command line parameters), find the subdirectories and determine the optimal combination.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "OptimalDVDSelectionBLL.hpp"
#include "CPredSameDirectory.hpp"
#include "CDVD.hpp"

namespace BLL {

static std::vector<CDirectory*> gEmptyDirectoryVector = {};

enum SELECTION_EXCEPTION
{
    SELECTION_EXCEPTION_INVALID_PRINTFNPTR
};

static std::map<SELECTION_EXCEPTION, std::string> gCommandLineRegExps = {
        {SELECTION_EXCEPTION_INVALID_PRINTFNPTR, "Please provide a valid Print function pointer."}
};

CSubDirectoriesBuilder::~CSubDirectoriesBuilder()
{}

bool ExceedsDVDSize(const double& size)
{ return size > CDVD::getMaxSize();}

bool ExceedsDVDSizeOrEmpty(const double& size)
{ return (ExceedsDVDSize(size)) || (size == 0.0);}

bool ExceedsDVDSizeOrEmpty(const CDirectory& dir)
{ return ExceedsDVDSizeOrEmpty(dir.getSize());}

bool ExceedsDVDSizeOrEmpty(const CCombination& comb)
{ return ExceedsDVDSizeOrEmpty(comb.getSize());}

bool DirectoryOrderAscending(const CDirectory& elem1, const CDirectory& elem2)
{ return elem1 > elem2;}

bool CombinationOrderAscending(const CCombination& elem1, const CCombination& elem2)
{ return elem1 > elem2;}

COptimalDVDSelection::COptimalDVDSelection(const CDVDSelectionCommandLineParser& dsclp, CSubDirectoriesBuilder* psdb)
    : m_CommandLine(dsclp), m_pfn(nullptr), m_NumberOfCombinations(0U), m_psdb(psdb), m_optimal(gEmptyDirectoryVector, 0U)
{
    this->m_psdb->AddSubDirectories(this->m_SubDirectories);

    // Removes directories that are larger than a DVD or are empty.
    std::vector<CDirectory>::iterator newit = std::remove_if(this->m_SubDirectories.begin(), this->m_SubDirectories.end(), static_cast<bool (*)(const CDirectory&)>(&ExceedsDVDSizeOrEmpty));

    this->m_SubDirectories.erase(newit, this->m_SubDirectories.end());

    // Sort.
    std::stable_sort(this->m_SubDirectories.begin(), this->m_SubDirectories.end(), DirectoryOrderAscending);
}

COptimalDVDSelection::~COptimalDVDSelection()
{}

// Print the optimal combination using the pfnPrintCombination function.
void COptimalDVDSelection::OutputOptimalCombinations(pfnPrintCombination pfn)
{
    switch(this->m_CommandLine.getType())
    {
    case OptimalCombinationsOutputType_Memory:
        this->findOptimalCombination(this->getK(), pfn);
        break;
    default:
        break;
    }
}

// Find and print the optimal combination of k elements using the pfnPrintCombination function.
void COptimalDVDSelection::findOptimalCombination(const CCombination::size_type& k, pfnPrintCombination pfn)
{
    this->m_pfn = pfn;
    this->m_NumberOfCombinations = 0U;
    std::vector<CCombination> combinations;

    // If k = 0, produces all possible combinations. Otherwise, only k combinations.
    for(CCombination::size_type selected_k = (k == 0U ? this->m_SubDirectories.size() : k); selected_k >= (k == 0U ? 1U : k); selected_k--)
        this->generateCombinations(k, combinations);

    this->m_NumberOfCombinations = combinations.size();

    // Removes combinations that are larger than a DVD or are empty.
    std::vector<CCombination>::iterator newit = std::remove_if(combinations.begin(), combinations.end(), static_cast<bool (*)(const CCombination&)>(&ExceedsDVDSizeOrEmpty));

    combinations.erase(newit, combinations.end());

    // Sort.
    std::stable_sort(combinations.begin(), combinations.end(), CombinationOrderAscending);

    // If there are combinations, print the optimal combination.
    if(combinations.size() != 0U)
    {   
        std::vector<CDirectory> optimizedComb;
        for(std::vector<CDirectory::size_type>::const_iterator itd = combinations[0].getDirectoriesIDs().begin(); itd != combinations[0].getDirectoriesIDs().end(); itd++)
        {
            // In order to save memory, a combination object has a vector of the subdirectories' IDs only.
            // Find all the subdirectories from the IDs and create a vector of directory objects to print as the result.
            std::vector<CDirectory>::const_iterator match;
            CDirectory dir(*itd, _T(""), 0.0);
            CPredSameDirectory pred(dir);

            match = std::find_if(this->m_SubDirectories.begin(), this->m_SubDirectories.end(), pred);

            if(match != this->m_SubDirectories.end())
                optimizedComb.push_back(*match);
        }

        this->m_pfn(optimizedComb, combinations[0].getID(), combinations[0].getSize());
        this->m_optimal = combinations[0];
    }
}

// Generate all possible combinations of k elements.
void COptimalDVDSelection::generateCombinations(const CCombination::size_type& k, std::vector<CCombination>& combinations)
{   // Adapted from http://rosettacode.org/wiki/Combinations#C.2B.2B
    // Using the pattern of a bitmask (selector) to generate all possible permutations of N elements in combinations of k elements.
    std::vector<CDirectory*> selected;
    std::vector<CCombination::size_type> selector(this->m_SubDirectories.size());
    std::fill(selector.begin(), selector.begin() + k, 1U);

    do {
        for (CCombination::size_type i = 0U; i < this->m_SubDirectories.size(); i++)
            if (selector[i] == 1U)
                selected.push_back(&(this->m_SubDirectories[i]));

        CCombination comb(selected, combinations.size() + 1U);
        combinations.push_back(comb);
        selected.clear();
    }while (std::prev_permutation(selector.begin(), selector.end()));
}

// Return the k elements used for each combination.
const CCombination::size_type& COptimalDVDSelection::getK(void) const
{ return this->m_CommandLine.getK(); }

// Return a vector of all subdirectories inside a directory.
const std::vector<CDirectory>& COptimalDVDSelection::getSubDirectories(void) const
{ return this->m_SubDirectories; }

// Return the number of generated combinations.
const CCombination::size_type& COptimalDVDSelection::getNumberOfCombinations(void) const
{ return this->m_NumberOfCombinations; }

// Return the object CCombination that is the optimal combination.
const CCombination& COptimalDVDSelection::getOptimalCombination() const
{return this->m_optimal;}

} /* namespace BLL */
