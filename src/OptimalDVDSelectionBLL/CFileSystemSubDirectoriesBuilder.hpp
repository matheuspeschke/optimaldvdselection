/**
    CFileSystemSubDirectoriesBuilder.hpp
    Purpose: identifies and sizes the subdirectories inside a directory (non recursive).

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include <vector>
#include "CDirectory.hpp"
#include "CSubDirectoriesBuilder.hpp"
#include "CDVDSelectionCommandLineParser.hpp"

namespace BLL {

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CFileSystemSubDirectoriesBuilder : public CSubDirectoriesBuilder {
protected:
    CDVDSelectionCommandLineParser m_commandline;
public:
    CFileSystemSubDirectoriesBuilder(CDVDSelectionCommandLineParser commandline);
public:
/**
    Find and size all subdirectories inside a directory.

    @param subDirs a vector of all found subdirectories.
*/
    void AddSubDirectories(std::vector<CDirectory>& subDirs);
};

}