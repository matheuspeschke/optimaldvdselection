/**
    CCommandLineParser.cpp
    Purpose: Superclass for managing parse of command line arguments

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "CCommandLineParser.hpp"

namespace BLL {

CCommandLineParser::CCommandLineParser(const std::map<unsigned int, const tstring>& args)
{this->m_parameters.insert(args.begin(), args.end());}

CCommandLineParser::~CCommandLineParser()
{}

} /* namespace BLL */
