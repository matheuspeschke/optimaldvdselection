/**
    CPredHelp.hpp
    Purpose: Predicate to parse command line help argument

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once
#include "CCommandLineParser.hpp"
#include "boost/filesystem.hpp"

namespace BLL
{

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CPredHelp
{
private:
    const CCommandLineParser::type_map_pair& m_candidate;
private:
    CPredHelp();
public:
    CPredHelp(const CCommandLineParser::type_map_pair& candidate);
    bool operator()(void);
/**
    Function operator - validate if command line parameter is a help parameter.

    @param candidate a command line parameter that might be a help parameter
    @return true (candidate is a help parameter) or false (candidate is not a help parameter)
*/
    bool operator()(const CCommandLineParser::type_map_pair& candidate);
};

}
