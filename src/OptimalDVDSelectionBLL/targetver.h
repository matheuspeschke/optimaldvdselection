/**
    targetver.h
    Purpose: Manage parsing of command line arguments

    Including SDKDDKVer.h defines the highest available Windows platform.
    If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
    set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#include <SDKDDKVer.h>