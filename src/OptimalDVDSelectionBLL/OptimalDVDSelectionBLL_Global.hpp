/**
    OptimalDVDSelectionBLL_Global.hpp
    Purpose: Manage visibility of importing and/or exporting this library.

    All files within this DLL are compiled with the OPTIMALDVDSELECTIONBLLSHARED_EXPORT
    symbol defined on the command line. This symbol should not be defined on any project
    that uses this DLL. This way any other project whose source files include this file see
    OPTIMALDVDSELECTIONBLL_API functions as being imported from a DLL, whereas this DLL sees symbols
    defined with this macro as being exported.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

//#include <QtCore/qglobal.h>
#  ifdef WIN32
#    define Q_DECL_EXPORT     __declspec(dllexport)
#    define Q_DECL_IMPORT     __declspec(dllimport)
#  else
#    define Q_DECL_EXPORT     __attribute__((visibility("default")))
#    define Q_DECL_IMPORT     __attribute__((visibility("default")))
#    define Q_DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif

#if defined(OPTIMALDVDSELECTIONBLL_LIBRARY)
#  define OPTIMALDVDSELECTIONBLLSHARED_EXPORT Q_DECL_EXPORT
#else
#  define OPTIMALDVDSELECTIONBLLSHARED_EXPORT Q_DECL_IMPORT
#endif
