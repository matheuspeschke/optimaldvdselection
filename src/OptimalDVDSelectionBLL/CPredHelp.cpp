/**
    CPredHelp.cpp
    Purpose: Predicate to parse command line help argument

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CPredHelp.hpp"

namespace BLL
{

static const tstring help(_T("help"));

CPredHelp::CPredHelp(const CCommandLineParser::type_map_pair& candidate)
: m_candidate(candidate)
{}

bool CPredHelp::operator()(void)
{return this->m_candidate.second == help;}

// Function operator - validate if command line parameter is a help parameter.
bool CPredHelp::operator()(const CCommandLineParser::type_map_pair& candidate)
{return candidate.second == help;}

}
