/**
    CDirectory.cpp
    Purpose: an instance of a filesystem directory information.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CDirectory.hpp"

namespace BLL {

CDirectory::CDirectory()
: m_ID(0U), m_Directory(_T("")), m_Size(0.0)
{}

CDirectory::CDirectory(const size_type& ID, const tstring& Directory, const double& Size)
: m_ID(ID), m_Directory(Directory), m_Size(Size)
{}

CDirectory::~CDirectory()
{}

const tstring& CDirectory::getDirectory(void) const
{return this->m_Directory;}

const double& CDirectory::getSize(void) const
{return this->m_Size;}

const CDirectory::size_type& CDirectory::getID(void) const
{ return this->m_ID;}

bool CDirectory::operator>(const CDirectory& rhs) const
{return this->getDirectory() > rhs.getDirectory();}

} /* namespace BLL */
