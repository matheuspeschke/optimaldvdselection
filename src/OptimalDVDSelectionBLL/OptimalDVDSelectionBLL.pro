#-------------------------------------------------
#
# Project created by QtCreator 2015-10-01T00:21:34
#
# 1. Make sure /usr/local/lib64/matheuspeschke is part of the path in /etc/ld.so.conf or /etc/ld.so.conf.d/ (apply changes using sudo ldconfig).
# 2. autoscan
# 3. cp configure.scan configure.ac
# 4. autoconf
# 5. ./configure
# 6. make
# 7. sudo make install
#-------------------------------------------------

QT       -= gui
QT       -= core

TARGET = OptimalDVDSelectionBLL
TEMPLATE = lib

CONFIG -= qt
CONFIG += c++11

DEFINES += OPTIMALDVDSELECTIONBLL_LIBRARY
DEFINES += BOOST_ALL_DYN_LINK

SOURCES += OptimalDVDSelectionBLL.cpp \
    stdafx.cpp \
    dllmain.cpp \
    CDirectory.cpp \
    CDVD.cpp \
    CSubDirectoriesBuilder.cpp \
    CFileSystemSubDirectoriesBuilder.cpp \
    CCommandLineParser.cpp \
    CDVDSelectionCommandLineParser.cpp \
    CException.cpp \
    CCombination.cpp \
    CPredHelp.cpp \
    CPredDirectoryExists.cpp \
    CPredSameDirectory.cpp

HEADERS += OptimalDVDSelectionBLL.hpp \
    OptimalDVDSelectionBLL_Global.hpp \
    tchar_templates.hpp \
    targetver.h \
    stdafx.h \
    CDirectory.hpp \
    CDVD.hpp \
    CSubDirectoriesBuilder.hpp \
    CFileSystemSubDirectoriesBuilder.hpp \
    CCommandLineParserTemplates.hpp \
    CCommandLineParser.hpp \
    CDVDSelectionCommandLineParser.hpp \
    CPredHelp.hpp \
    CPredDirectoryExists.hpp \
    CPredSameDirectory.hpp \
    CException.hpp \
    CCombination.hpp

PRECOMPILED_HEADER = stdafx.h

!win32{
    target.path = /usr/local/lib64/optimaldvdselection
    INSTALLS += target
    LIBS += -lboost_filesystem -lboost_system -lboost_regex
    CONFIG(debug, debug|release){
        QMAKE_CXXFLAGS += --coverage
        QMAKE_LFLAGS += --coverage
    }
    else{
        QMAKE_CXXFLAGS -= --coverage
        QMAKE_LFLAGS -= --coverage
    }
}
else{
    QMAKE_CXXFLAGS += /Zi
    QMAKE_LFLAGS += /INCREMENTAL:NO /Debug
}

VERSION = 2.0.0
