/**
    CDirectory.hpp
    Purpose: an instance of a filesystem directory information.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#include "OptimalDVDSelectionBLL_Global.hpp"
#include <vector>

#ifndef _WINDOWS
#include "tchar_templates.hpp"
#endif

namespace BLL {

class OPTIMALDVDSELECTIONBLLSHARED_EXPORT CDirectory
{
public:
    typedef std::vector<CDirectory*>::size_type size_type;
private:
    // An ID.
    size_type m_ID;
    // Directory's full path.
    tstring m_Directory;
    // Size in Megabytes.
    double m_Size;
public:
    const tstring& getDirectory(void) const;
    const double& getSize(void) const;
    const size_type& getID(void) const;
public:
    CDirectory();
    CDirectory(const size_type& ID, const tstring& Directory, const double& Size);
    virtual ~CDirectory();
public:
    bool operator>(const CDirectory& rhs) const;
};

} /* namespace BLL */
