/**
    CPredSameDirectory.cpp
    Purpose: Predicate to compare two directories by their IDs.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CPredSameDirectory.hpp"

namespace BLL
{

CPredSameDirectory::CPredSameDirectory(const CDirectory& candidate)
: m_candidate(candidate)
{}

// Function operator - compare two CDirectory objects by their IDs.
bool CPredSameDirectory::operator()(const CDirectory& candidate)
{return this->m_candidate.getID() == candidate.getID();}

}
