/**
    CDVD.cpp
    Purpose: inform the max size of a DVD media (http://en.wikipedia.org/wiki/DVD).

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"
#include "CDVD.hpp"

namespace BLL {

CDVD::CDVD()
{}

// Max size of a DVD disk.
const double& CDVD::getMaxSize(void)
{
    // Size in Megabytes. See 'Capacity' in http://en.wikipedia.org/wiki/DVD.
    static const double maxsize = 4.7*1000.0; // For some bizarre reason, most media burners forget to use 2^10 (1024) when multiplying bytes. They use 1000 instead;
    return maxsize;
}

CDVD::~CDVD()
{}

} /* namespace BLL */
