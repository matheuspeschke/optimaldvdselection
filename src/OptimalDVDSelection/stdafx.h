/**
    stdafx.h
    Purpose: Pre-compiled header.

    Reference any additional headers you need here.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#pragma once

#ifdef WIN32
#include "targetver.h"
#include <memory>

#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, THIS_FILE, __LINE__)
#define _CRTDBG_MAP_ALLOC 	// include Microsoft memory leak detection procedures.
#define _INC_MALLOC			// exclude standard memory alloc procedures.
#include <crtdbg.h>

#endif
#include <tchar.h>
#endif

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <CFileSystemSubDirectoriesBuilder.hpp>
#include <OptimalDVDSelectionBLL.hpp>
