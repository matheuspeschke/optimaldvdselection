# 1. autoscan
# 2. cp configure.scan configure.ac
# 3. autoconf
# 4. ./configure
# 5. make
# 6. sudo make install

QT -= gui
QT -= core

TARGET = OptimalDVDSelection
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += BOOST_ALL_DYN_LINK

SOURCES += main.cpp \
    stdafx.cpp

HEADERS += \
    stdafx.h \
    targetver.h

PRECOMPILED_HEADER = stdafx.h

!win32{
    target.path = /usr/local/bin
    INCLUDEPATH += ../OptimalDVDSelectionBLL
    INSTALLS += target
    LIBS += -lboost_filesystem -lboost_system -lboost_regex
    LIBS += -L../OptimalDVDSelectionBLL/ -lOptimalDVDSelectionBLL
    CONFIG(debug, debug|release){
        QMAKE_CXXFLAGS += --coverage
        QMAKE_LFLAGS += --coverage
    }
    else{
        QMAKE_CXXFLAGS -= --coverage
        QMAKE_LFLAGS -= --coverage
    }
}
else{
    QMAKE_CXXFLAGS += /Zi
    QMAKE_LFLAGS += /INCREMENTAL:NO /Debug
}
