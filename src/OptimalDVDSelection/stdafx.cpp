/**
    stdafx.cpp
    Purpose: Pre-compiled header.

    Reference any additional headers you need in stdafx.h and not in this file

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"