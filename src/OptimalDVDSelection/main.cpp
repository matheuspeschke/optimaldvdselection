/**
    main.cpp
    Purpose: Captures the parameters via command line, instantiate the OptimalDVDSelectionBLL
    objects and generate/print the optimal combination.

    @author Matheus Peschke
    @version 2.0.0 2020-05-21
*/
#include "stdafx.h"

#ifndef WIN32
#define _tmain main
#else
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

using namespace BLL;

void PrintCombination(const std::vector<BLL::CDirectory>& combination, const CCombination::size_type& combID, const double& size)
{
    tstring semicolon(_T(";"));

    // To the standard output, so can be saved as a CSV file, if needed.
    for(CCombination::size_type i = 0U; i < combination.size(); i++)
        tcout << combID << semicolon << combination[i].getDirectory() << semicolon << combination[i].getSize() << semicolon << size << std::endl;
}

int _tmain(int argc, ::TCHAR* argv[])
{
    try
    {
        std::map<unsigned int, const tstring> params;
        for(CCombination::size_type i = 0U; i < static_cast<CCombination::size_type>(argc); i++)
            params.insert({i, tstring(argv[i])});

        CDVDSelectionCommandLineParser csclp(params);
        CFileSystemSubDirectoriesBuilder fssdb(csclp);

        COptimalDVDSelection opt(csclp, &fssdb);

        opt.OutputOptimalCombinations(PrintCombination);
    }
    catch(BLL::CExceptionCommandLineParser& e)
    {
        tcout << e.what() << std::endl;
        return 1;
    }
    catch(BLL::CException& e)
    {
        tcout << e.what() << std::endl;
        return 3;
    }
    catch(std::exception& e)
    {
        tcout << e.what() << std::endl;
        return 4;
    }

    return 0;
}
