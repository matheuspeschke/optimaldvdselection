# OptimalDVDSelection #

OptimalDVDSelection is a command-line tool that calculates the optimal combination of subdirectories to fit into a blank DVD-R media (4.7 GB) - 'optimal combination' meaning the combination with minimal unused media storage space. For a definition of 'combination', see: https://en.wikipedia.org/wiki/Combination  

The output of this tool is the content of a CSV file (https://en.wikipedia.org/wiki/Comma-separated_values), but using semicolons instead of commas.  

## Best fit for a DVD media ##

Let's assume that your current directory (the set 'S') has the following 4 ('n') subdirectories:  

subdir1 (1024 MB)  
subdir2 (2048 MB)  
subdir3 (4096 MB)  
subdir4 (1024 MB)  

Running the tool on your current directory to identify what is the best combination for 3 ('k') subdirectories would output:

3;subdir1;1024;4096  
3;subdir2;2048;4096  
3;subdir4;1024;4096

*First column*: the sequential number of this combination of 3 (k) subdirectories (of all possible 'n' combinations)  
*Second column*: the subdirectory of this combination  
*Third column*: the size of the subdirectory  
*Fourth column*: the size of the optimal combination  

Don't underestimate the apparent simplicity of the algorithm. One of the major challenges is to use as few memory as possible when both the set (S) and k elements are large numbers, which can lead to millions or even billions of possible unique combinations - exhausting your available physical memory. You can check the memory usage when running the unit tests (see below on section 'Running unit tests')  

# Development Guidelines #

If using a C++ IDE (Visual Studio Code, Qt, etc) it will detect the CMakeLists.txt files and guide you through configuring the C++ compiler and the CMake/CTest features (*this project supports the following compilers: CLang = 6.0 and GCC >= 7.5.0*).  
The instructions below can be applied directly from the command line, without an IDE.  

*Note: tests for CLang used a specific version, 6.0. In case you want to try a different version, make sure you edit the llvm-gcov.sh file to use your llvm-cov tool*.  

## Local dependencies ##

To install local dependencies (compiler: CLang), run the command:  

*\# ./Ubuntu-18-Dependencies-CLANG.sh*  

To install local dependencies (compiler: GCC), run the command:  

*\# ./Ubuntu-18-Dependencies-GCC.sh*  

## Building ##

To build the tool, run the command:  

*$ ./Ubuntu-18-Build.sh*  

## Running unit tests ##

To execute the unit tests, run the commands:

*$ cd build/*  
*$ ctest -V*  

Output (example):

test 1  
    Start 1: unittests  

1: Test command: < cmake build dir >/tests/OptimalDVDSelectionBLL-Unit-Tests "--log_level=message"  
1: Test timeout computed to be: 9.99988e+06  
1: Running 26 test cases...  
1: 2;unittest-dummy-subdir-4;1024;3072  
1: 2;unittest-dummy-subdir-2;2048;3072  
1: Absolute difference of generated DVD size (3072.000000) and provided literal (3072.000000): 0.000000  
1: Acceptable double distance: 0.010000  
1: 3;unittest-dummy-subdir-4;1024;4096  
1: 3;unittest-dummy-subdir-2;2048;4096  
1: 3;unittest-dummy-subdir-1;1024;4096  
1: Absolute difference of generated DVD size (4096.000000) and provided literal (4096.000000): 0.000000  
1: Acceptable double distance: 0.010000  
1: 4767;unittest-dummy-subdir-36;184.161;4699.73  
1: 4767;unittest-dummy-subdir-15;2437.68;4699.73  
1: 4767;unittest-dummy-subdir-1;2077.89;4699.73  
1: Absolute difference of generated DVD size (4699.731000) and provided literal (4699.730000): 0.001000  
1: Acceptable double distance: 0.010000  
1: 280218;unittest-dummy-subdir-4;2101.86;4699.99  
1: 280218;unittest-dummy-subdir-34;957.482;4699.99  
1: 280218;unittest-dummy-subdir-33;224.426;4699.99  
1: 280218;unittest-dummy-subdir-28;1026.48;4699.99  
1: 280218;unittest-dummy-subdir-17;389.74;4699.99  
1: Absolute difference of generated DVD size (4699.988000) and provided literal (4699.990000): 0.002000  
1: Acceptable double distance: 0.010000  
1: 4380631;unittest-dummy-subdir-7;1279.98;4700  
1: 4380631;unittest-dummy-subdir-6;439.878;4700  
1: 4380631;unittest-dummy-subdir-38;714.821;4700  
1: 4380631;unittest-dummy-subdir-19;645.363;4700  
1: 4380631;unittest-dummy-subdir-17;389.74;4700  
1: 4380631;unittest-dummy-subdir-16;613.415;4700  
1: 4380631;unittest-dummy-subdir-12;616.803;4700  
1: Absolute difference of generated DVD size (4700.000000) and provided literal (4700.000000): 0.000000  
1: Acceptable double distance: 0.010000  
1:  
1: *** No errors detected  

## Code coverage ##

a. Generate the report of unit test code coverage:  

For CLang:  
*$ ./LCOV-generate-code-coverage-report-clang.sh*  

For GCC:  
*$ ./LCOV-generate-code-coverage-report-gcc.sh*  

b. Look for the HTML code coverage report in:  

For CLang:  
*${PWD}/build-clang/coveragedir/index.html*  

For GCC:  
*${PWD}/build-gcc/coveragedir/index.html*  

## Running Code Quality checking ##

N/A  

## Running as a Docker container ##

a. Build the Docker image:  

*$ cd ..*  

*\# docker build --tag optimaldvdselection:develop .*  

b. Run the container, mapping the volume to your current directory and defining a 'k':  

*\# docker run --rm --name test -v \$PWD:/opt/optimaldvdselection/dvd optimaldvdselection:develop k=3*  

## Generate documentation ##

a. You must have doxygen and graphviz installed. Run the commands:  

*\# apt update*  
*\# apt install -y doxygen doxygen-doc graphviz graphviz-doc*  

b. Edit the 'Doxygen' file's variables: OUTPUT_DIRECTORY and INPUT to reflect the paths of your cloned repository.  

*$ doxygen Doxyfile*  

# Contribution guidelines #

This project uses Boost Unit Test module to validate the implementation. Bitbucket pipelines build and deploy the Docker image to Docker Hub (https://hub.docker.com/r/mpeschke/optimaldvdselection).  

a. Ask the repo owner to be included in the Collaborators group (you must have a valid Bitbucket account (email)).

b. Branch from Develop branch to a new Feature branch.

c. Create the fixtures and test cases.

d. Open a pull request to the Develop branch.